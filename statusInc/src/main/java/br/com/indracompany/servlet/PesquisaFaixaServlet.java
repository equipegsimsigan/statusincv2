package br.com.indracompany.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.indracompany.dao.AtlysDAO;
import br.com.indracompany.dao.SiganDAO;
import br.com.indracompany.process.FAIXAPO;

/**
 * Servlet implementation class PesquisaFaixaServlet
 */
@WebServlet(name = "PesquisaFaixaServlet", urlPatterns = {"/PesquisaFaixaServlet"})
public class PesquisaFaixaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException, ServletException {

		String faixa = request.getParameter("faixa").trim();
		
		faixa = faixa.replace("(", "");
		faixa = faixa.replace(")", "");
        faixa = faixa.replace(" ", "");
        faixa = faixa.replace("-", "");

		String ddd = "";
		String prefixo = "";
		String sufixo = "";
		
		
		int tamanhoFaixa = faixa.length();
		
		switch(tamanhoFaixa) {
			case 11: 
				
				for(int i = 0; i <= tamanhoFaixa; i++) {
					if(tamanhoFaixa <= 20) {
						if(i == 0 || i == 1) {
							ddd = ddd + faixa.charAt(i);
						}
						if(i >= 2 && i <= 6) {
							prefixo = prefixo + faixa.charAt(i);
							}
						if(i >= 7 && i <= 10) {
							sufixo = sufixo + faixa.charAt(i);
						}
					}
				}
				break;
				
		case 10: 
			for(int i = 0; i < tamanhoFaixa; i++) {
				
				if(tamanhoFaixa <= 20) {
					if(i == 0 || i == 1) {
					ddd = ddd + faixa.charAt(i);
					}
					if(i >= 2 && i <= 5) {
						prefixo = prefixo + faixa.charAt(i);
						}
					if(i >= 6 && i <= 10) {
						sufixo = sufixo + faixa.charAt(i);
			
					}
				}
			}
		
			break;
		}
				
		
		ArrayList<FAIXAPO> faixaResult = new ArrayList<FAIXAPO>();
		
		faixaResult = SiganDAO.getFaixa(ddd,prefixo,sufixo);
		
		
		request.setAttribute("faixa", faixaResult);
		
		
		ArrayList<FAIXAPO> faixaNoAtlys = new ArrayList<FAIXAPO>();
		
		faixaNoAtlys = AtlysDAO.getFaixaAtlys(ddd,prefixo);
		
		request.setAttribute("faixaAtlys", faixaNoAtlys);
		
		request.getRequestDispatcher("index.jsp").forward(request, response);
		
		
	}
		
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
			processRequest(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
			processRequest(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	

}