package br.com.indracompany.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.indracompany.dao.AtlysDAO;
import br.com.indracompany.dao.BancoSPN;
import br.com.indracompany.dao.GSIMDAO;
import br.com.indracompany.dao.NginDAO;
import br.com.indracompany.dao.SiganDAO;
import br.com.indracompany.process.AtlysPO;
import br.com.indracompany.process.GSIMPO;
import br.com.indracompany.process.NGINPO;
import br.com.indracompany.process.ResultadosPO;
import br.com.indracompany.process.SIGANEVENTOSPO;
import br.com.indracompany.process.SIGANPO;
import br.com.indracompany.process.SPNPO;

/**
 * Servlet implementation class PesquisaLinhaServlet
 */
@WebServlet(name = "PesquisaLinhaServlet", urlPatterns = {"/PesquisaLinhaServlet"})
public class PesquisaLinhaServlet extends HttpServlet {
	 
       
    /**
     * @return 
     * @throws IOException 
     * @throws SQLException 
     * @throws ServletException 
     * @see HttpServlet#HttpServlet()
     */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException, ServletException {
    	
    	response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            //TODO: fazer pesquisa com banco de dados
            
            String linha = request.getParameter("linha").trim();

            linha = linha.replace("(", "");
            linha = linha.replace(")", "");
            linha = linha.replace(" ", "");
            linha = linha.replace("-", "");
            
            
            //linha = linha.substring();
            
            String iccid = request.getParameter("iccid").trim();
            
            
            ArrayList <SIGANPO> listaLinhasSIGAN = new ArrayList();
     		ArrayList <SIGANEVENTOSPO> listaLinhasSIGANEVENTOS = new ArrayList();
     		
     		GSIMPO gsimmsisdn = new GSIMPO();
     		NGINPO ngin = new NGINPO();
     		AtlysPO atlys = new AtlysPO();
     		SPNPO spn = new SPNPO(); 
     		SIGANPO sigan = new SIGANPO();
     		GSIMPO gsimiccid = new GSIMPO();
     		GSIMPO gsimblacklist = new GSIMPO();
     		ArrayList<GSIMPO> gsimhis = new ArrayList<>();
    		ResultadosPO resultados = new ResultadosPO();
     		
    		
    		
    		
     		if(linha != null || !linha.equals("")) {
        	
     			ngin = NginDAO.getLinha(linha);
        	
     			atlys = AtlysDAO.getLinha(linha);
  
     			spn = BancoSPN.getLinhaSPN(linha);
        	
     			gsimmsisdn = GSIMDAO.getICCIDByMSISDN(linha);  
     			
     			sigan = SiganDAO.getLinhaSTATUS(linha);
     		
	     		listaLinhasSIGAN  = SiganDAO.getLinhaHIST(linha);
	            
	    		listaLinhasSIGANEVENTOS  = SiganDAO.getLinhaEVENTOS(linha);
	    		
	    		gsimblacklist = GSIMDAO.getICCIDBlackList(linha);
	    		
	    		if (iccid.equals("") && !gsimmsisdn.getICCID().equals("")) {
	    			iccid = gsimmsisdn.getICCID();
	    		}
        	}
    		   
     		if(iccid != null && !iccid.equals("")) {    		
	    		gsimiccid = GSIMDAO.getICCID(iccid);	    			
	    		gsimhis = GSIMDAO.getICCIDHist(iccid);	    	

     		}
    		if(linha != null && !linha.equals("")) {
        		if(!gsimmsisdn.getICCID().equals(gsimiccid.getICCID())) {
        			resultados.setSimcard(true);   
        		}
    		}
 
    		if(linha.equals("") || linha == null && gsimiccid.getMSISDN() != null) {
    			linha = gsimiccid.getMSISDN();
    			
    		}
    		
    		if(linha != null && !gsimiccid.getMSISDN().equals("000000000000")  && gsimiccid.getMSISDN() != null && !gsimiccid.getMSISDN().equals("")) {
    			
    			linha = gsimiccid.getMSISDN().substring(2);
    			
    			
    			
    			ngin = NginDAO.getLinha(linha);
            	
     			atlys = AtlysDAO.getLinha(linha);
  
     			spn = BancoSPN.getLinhaSPN(linha);
        	
     			gsimmsisdn = GSIMDAO.getICCIDByMSISDN(linha);        	

     			sigan = SiganDAO.getLinhaSTATUS(linha);
     			
	     		listaLinhasSIGAN  = SiganDAO.getLinhaHIST(linha);
	            
	    		listaLinhasSIGANEVENTOS  = SiganDAO.getLinhaEVENTOS(linha);
	    		
	    		gsimblacklist = GSIMDAO.getICCIDBlackList(linha);
	    		
    		} else {
    			linha = "1";
    		}
    		
			 
//			    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
//			    Date date = new Date();
//			    date.set(dd)
//			    return dateFormat.format(date);
//			
//    		
//    		if(sigan.getDTFUTURA() >= date) {
//    			
//    		}
    		
    		resultados.setNgin(ngin);
    		resultados.setAtlys(atlys);
    		resultados.setSpn(spn);
    		resultados.setGsimhis(gsimhis);
    		resultados.setGsimblacklist(gsimblacklist);
    		resultados.setSiganhist(listaLinhasSIGAN);
    		resultados.setSigan(sigan);
    		resultados.setSiganeventos(listaLinhasSIGANEVENTOS);
    		resultados.setGsimmsisdn(gsimmsisdn);
    		resultados.setGsimiccid(gsimiccid);
    		
           
    		// response.sendRedirect("Projetinho.jsp", resultados);
           
        	request.setAttribute("resultados", resultados);

    		// Forward to to the JSP file.
    	    request.getRequestDispatcher("index.jsp").forward(request, response);
        }
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
			processRequest(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
			processRequest(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>


}
