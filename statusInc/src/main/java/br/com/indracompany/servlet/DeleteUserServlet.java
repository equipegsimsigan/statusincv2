package br.com.indracompany.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.indracompany.dao.SiganDAO;


@WebServlet(name = "DeleteUserServlet", urlPatterns = {"/DeleteUserServlet"})
public class DeleteUserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    
 /**
  * @throws IOException 
  * @throws SQLException 
  * @throws ServletException 
  * @see HttpServlet#HttpServlet()
  */

       
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException, ServletException {
		// programe aqui
		SiganDAO.siganMatarSessao();
		
		///
		
		
		request.setAttribute("SessaoOK", true);
		
		request.getRequestDispatcher("index.jsp").forward(request, response);

    }
	
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
			processRequest(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
			processRequest(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}
