package br.com.indracompany.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.indracompany.dao.AtlysDAO;
import br.com.indracompany.dao.SiganDAO;
import br.com.indracompany.process.FAIXAPO;

/**
 * Servlet implementation class PesquisaFaixaServlet
 */
@WebServlet(name = "PesquisaFaixaInteiraServlet", urlPatterns = {"/PesquisaFaixaInteiraServlet"})
public class PesquisaFaixaInteiraServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws IOException, SQLException, ServletException {

		
		String faixa = request.getParameter("faixa").trim();
		
		faixa = faixa.replace("%", "");
		faixa = faixa.replace("28", "");
        faixa = faixa.replace("29", "");
        faixa = faixa.replace("+", "");
        faixa = faixa.replace("-", "");
		
		
		String ddd = "";
		String prefixo = "";
		
		int tamanhoFaixa = faixa.length();
		
		switch(tamanhoFaixa) {
		case 11: 
			
			for(int i = 0; i < tamanhoFaixa; i++) {
				if(tamanhoFaixa <= 20) {
					if(i == 0 || i == 1) {
						ddd = ddd + faixa.charAt(i);
					}
					if(i >= 2 && i <= 6) {
						prefixo = prefixo + faixa.charAt(i);
					}
				}
			}
			break;
			
	case 10: 
		for(int i = 0; i < tamanhoFaixa; i++) {
			
			if(tamanhoFaixa <= 20) {
				if(i == 0 || i == 1) {
				ddd = ddd + faixa.charAt(i);
				}
				if(i >= 2 && i <= 5) {
					prefixo = prefixo + faixa.charAt(i);
					}
			}
		}
	
		break;
	}
		
		
		
//		for(int i = 0; i <= tamanhoFaixa; i++) {
		
//			if(tamanhoFaixa < 20) {
//				if(i == 0 || i == 1) {
//					ddd = ddd + faixa.charAt(i);
//				}
//				if(i >= 2 && i <= 6) {
//					prefixo = prefixo + faixa.charAt(i);
//				}		
//				}else {
//				if(i == 0 || i == 1) {
//					ddd = ddd + faixa.charAt(i);
//				}
//				if(i >= 2 && i <= 5) {
//					prefixo = prefixo + faixa.charAt(i);
//				
//				}
//				}
//		}
			
		
		ArrayList<FAIXAPO> faixaResult = new ArrayList<>();
		
		faixaResult = SiganDAO.getFaixaInteira(ddd,prefixo);
		
		request.setAttribute("faixainteira", faixaResult);
		
		ArrayList<FAIXAPO> faixaInteiraAtlys = new ArrayList<>();
		
		faixaInteiraAtlys = AtlysDAO.getFaixaInteiraAtlys(ddd, prefixo);
		
		request.setAttribute("faixaIntAtlys", faixaInteiraAtlys);
				
		request.getRequestDispatcher("index.jsp").forward(request, response);
		
		}
	
	
    private void FAIXAPO() {
		// TODO Auto-generated method stub
		
	}

	@Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
			processRequest(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
			processRequest(request, response);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
	

}
