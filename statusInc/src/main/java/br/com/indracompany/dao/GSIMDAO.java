package br.com.indracompany.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.indracompany.process.GSIMPO;

public class GSIMDAO {
	
	
	public static GSIMPO getICCIDByMSISDN( String linha) throws SQLException{
		
		
    	Connection con = Conexao.conectarGSIM();
    	GSIMPO gsim = new GSIMPO();
	
		String sql = "select sc.iccid, sc.msisdn, sc.imsi, sc.status_code, st.status_description, sc.date_new, sc.usr_new from gsim.simcard sc, GSIM.simcard_status st where sc.status_code(+) = st.status_code AND sc.msisdn = ?";
	 
		try {
	    	PreparedStatement ps = con.prepareStatement(sql);
		    
		    ps.setString(1, "55" + linha);
		    
		    ResultSet rs = ps.executeQuery();
		    if(rs.next()) {
		    	gsim.setICCID(rs.getString("ICCID"));
		    	gsim.setMSISDN(rs.getString("MSISDN"));
		    	gsim.setIMSI(rs.getString("IMSI"));
		    	gsim.setSTATUSCODE(rs.getString("STATUS_CODE"));
		    	if(rs.getString("STATUS_CODE") != null)
		    		gsim.setSTATUS_CODE(rs.getString("STATUS_CODE") + " - " + rs.getString("STATUS_DESCRIPTION"));
		    	else
		    		gsim.setSTATUS_CODE("-");
		    	gsim.setSTATUS_DESCRIPTION(rs.getString("STATUS_DESCRIPTION"));
		    	gsim.setDATE_NEW(rs.getString("DATE_NEW"));
		    	gsim.setUSR_NEW(rs.getString("USR_NEW"));
		    } else {
		    	System.out.println("Resultados não encontrados!!!!!");
		    }
		    	
	    }catch (Exception e) {
			e.printStackTrace();
		} finally {
			con.close();
		}

	return gsim;	
	}

	public static GSIMPO getICCID(String iccid) throws SQLException {
		Connection con = Conexao.conectarGSIM();
    	GSIMPO gsimiccid = new GSIMPO();
	
		String sql = "select sc.iccid, sc.msisdn, sc.IMSI , /*sc.ID_REMOTE_ENABLE_LOT_ORDER ,*/sc.date_new, sc.status_code, sc.USR_NEW, s.STATUS_DESCRIPTION, " + 
				"combo, sc.ID_REMOTE_ENABLE_LOT_ORDER,sc.ID_INPUT_FILE, sc.id_denatran_lot_order, sc.ID_ICCID_PATTERN " + 
				"from gsim.simcard sc, gsim.simcard_status s    " + 
				"where s.STATUS_CODE = sc.STATUS_CODE(+) and sc.iccid IN " + 
				"(?)";
	 
		try {
	    	PreparedStatement ps = con.prepareStatement(sql);
		    
		    ps.setString(1, iccid);
		    
		    ResultSet rs = ps.executeQuery();
		    if(rs.next()) {
		    	gsimiccid.setICCID(rs.getString("ICCID"));
		    	gsimiccid.setMSISDN(rs.getString("MSISDN"));
		    	gsimiccid.setIMSI(rs.getString("IMSI"));
		    	gsimiccid.setSTATUSCODE(rs.getString("STATUS_CODE"));
		    	gsimiccid.setSTATUS_DESCRIPTION(rs.getString("STATUS_DESCRIPTION"));
		    	gsimiccid.setDATE_NEW(rs.getString("DATE_NEW"));
		    	gsimiccid.setUSR_NEW(rs.getString("USR_NEW"));
		    } else {
		    	System.out.println("Resultados não encontrados!!!!!");
		    }
		    	
	    }catch (Exception e) {
			e.printStackTrace();
		} finally {
			con.close();
		}

	return gsimiccid;
	}

	public static ArrayList<GSIMPO> getICCIDHist(String iccid) throws SQLException {
		Connection con = Conexao.conectarGSIM();
		
		ArrayList <GSIMPO> ListaICCIDHIS = new ArrayList();
		
    
	
		String sql = "select h.iccid, h.msisdn, h.IMSI, h.date_new, h.USR_NEW , h.status_code, s.STATUS_DESCRIPTION, h.ID_ICCID_PATTERN " + 
				"from gsim.simcard_hist h, GSIM.SIMCARD_STATUS s  " + 
				"where h.STATUS_CODE = s.STATUS_CODE and h.iccid in  " + 
				"( " + 
				"?" + 
				")  " + 
				"order by h.date_new desc, id_simcard_hist desc";
	 
		try {
	    	PreparedStatement ps = con.prepareStatement(sql);
		    
		    ps.setString(1, iccid);
		    
		    ResultSet rs = ps.executeQuery();
		    while(rs.next()) {
		    	GSIMPO gsimhis = new GSIMPO();
		    	gsimhis.setICCID(rs.getString("ICCID"));
		    	gsimhis.setMSISDN(rs.getString("MSISDN"));
		    	gsimhis.setIMSI(rs.getString("IMSI"));
		    	gsimhis.setSTATUSCODE(rs.getString("STATUS_CODE"));
		    	gsimhis.setSTATUS_DESCRIPTION(rs.getString("STATUS_DESCRIPTION"));
		    	gsimhis.setDATE_NEW(rs.getString("DATE_NEW"));
		    	gsimhis.setUSR_NEW(rs.getString("USR_NEW"));
		    	
		    	ListaICCIDHIS.add(gsimhis);
		    } 
		    	
	    }catch (Exception e) {
			e.printStackTrace();
		} finally {
			con.close();
		}

	return ListaICCIDHIS;	
	}

	public static GSIMPO getICCIDBlackList(String linha) throws SQLException {
		Connection con = Conexao.conectarGSIM();
		
    	GSIMPO Gsimblacklist = new GSIMPO();
	
		String sql = "Select * from GSIM.black_list where msisdn = ?";
	 
		try {
	    	PreparedStatement ps = con.prepareStatement(sql);
		    
		    ps.setString(1,"55" + linha);
		    
		    ResultSet rs = ps.executeQuery();
		    	if(rs.next()) {
		    	Gsimblacklist.setID_BLACK_LIST(rs.getString("ID_BLACK_LIST"));
		    	Gsimblacklist.setMSISDN(rs.getString("MSISDN"));
		    	Gsimblacklist.setSTATUS(rs.getString("STATUS"));
		    	Gsimblacklist.setUSR_NEW(rs.getString("USR_NEW"));
		    	Gsimblacklist.setDATE_NEW(rs.getString("DATE_NEW"));
		    	Gsimblacklist.setUSR_UPDATE(rs.getString("USR_UPDATE"));
		    	Gsimblacklist.setDATE_UPDATE(rs.getString("DATE_UPDATE"));
		    } 
		    	
	    }catch (Exception e) {
			e.printStackTrace();
		} finally {
			con.close();
		}

	return Gsimblacklist;	
	}
}
	
	

	