package br.com.indracompany.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.indracompany.process.SIGANPO;

public class EventosDAO {
	
	
	public static ArrayList<SIGANPO> getLinha( String linha) throws SQLException{
		
		ArrayList<SIGANPO> listaSIGANEVENTOS = new ArrayList();
		
    	Connection con = Conexao.conectarSIGAN();
    	SIGANPO siganev = new SIGANPO();
	
		String sql = "SELECT LAE.*, ev.dsevento FROM ( " + 
				"SELECT  PREFIXONUMERO, DTTRANSACAO, IDSISTEMAORIGEM, IDSISTEMADESTINO, DSERRO, IDFUNCIONALIDADE FROM SIGAN_OW.LOGAVISOERRO  " + 
				"WHERE PREFIXONUMERO = '?' " + 
				"ORDER BY DTTRANSACAO DESC " + 
				") LAE, SIGAN_OW.EVENTO EV WHERE SUBSTR(LAE.DSERRO, 9,2) = ev.idevento(+)";
	 
		try {
	    	PreparedStatement ps = con.prepareStatement(sql);
		    
		    ps.setString(1, "55"+linha);
		    
		    ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				siganev.setPREFIXONUMERO(rs.getString("PREFIXONUMERO"));
		    	siganev.setDTALTERACAOEVENTO(rs.getString("DTTRANSACAO"));
		    	siganev.setIDSISTEMAORIGEM(rs.getString("IDSISTEMAORIGEM"));
		    	siganev.setIDSISTEMADESTINO(rs.getString("IDSISTEMADESTINO"));
		    	siganev.setDSERRO(rs.getString("DSERRO"));
		    	siganev.setIDFUNCIONALIDADE(rs.getString("IDFUNCIONALIDADE"));
			} 	
	    }catch (Exception e) {
			e.printStackTrace();
		} finally {
			con.close();
		}

	return listaSIGANEVENTOS;	
	}

}
	
	

	