package br.com.indracompany.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.indracompany.process.FAIXAPO;
import br.com.indracompany.process.SIGANEVENTOSPO;
import br.com.indracompany.process.SIGANPO;

public class SiganDAO {
	
	
	public static SIGANPO getLinhaSTATUS( String linha) throws SQLException{
		
    	SIGANPO sigan = new SIGANPO();
		
    	Connection con = Conexao.conectarSIGAN();
	
		String sql = "SELECT N.IDMSISDN, N.IDSTATUS, ST.DSSTATUS, S.NMSISTEMA, N.IDSISTEMAATUAL, TN.DSTIPONUMERO, N.DTFUTURA, N.DTULTIMAALTERACAO, L.DSLOCALIDADE, " + 
				" N.LOGIN, N.CNL, N.IDLOCALIDADE, N.INNUMERO, N.IDCATEGORIAATUAL, C.NMCATEGORIA, N.IDTPREDE, TR.DSTPREDE" + 
				" FROM SIGAN_OW.NUMERO N, SIGAN_OW.STATUSNUMERO ST, SIGAN_OW.SISTEMA S, SIGAN_OW.TIPOREDE TR, SIGAN_OW.CATEGORIANUMERO C, SIGAN_OW.TIPONUMERO TN, SIGAN_OW.LOCALIDADE L" + 
				" WHERE N.IDSTATUS = ST.IDSTATUS AND" + 
				" N.IDLOCALIDADE = L.IDLOCALIDADE AND " +
				" N.IDSISTEMAATUAL = S.IDSISTEMA(+) AND" + 
				" N.IDCATEGORIAATUAL = C.IDCATEGORIANUMERO AND" + 
				" N.IDTPREDE = TR.IDTPREDE AND" + 
				" N.IDTIPONUMERO = TN.IDTIPONUMERO(+) AND" + 
				" N.IDMSISDN = ?"; 
	 
		try {
	    	PreparedStatement ps = con.prepareStatement(sql);
		    
		    ps.setString(1, "55" + linha);
		    
		    ResultSet rs = ps.executeQuery();
		    while(rs.next()) {
		    	sigan.setIDMSISDN(rs.getString("IDMSISDN"));
		    	sigan.setIDSTATUS(rs.getString("IDSTATUS"));
		    	
		    	if(rs.getString("IDSTATUS") != null)
		    		sigan.setIDSTATUS(rs.getString("IDSTATUS") + " - " + rs.getString("DSSTATUS"));
		    	else
		    		sigan.setIDSTATUS("-");
		    	
		    	sigan.setDSSTATUS(rs.getString("DSSTATUS"));
		    	
		    	sigan.setNMSISTEMA(rs.getString("NMSISTEMA"));
		    	if(rs.getString("IDSISTEMAATUAL") != null)
		    		sigan.setIDSISTEMAATUAL(rs.getString("IDSISTEMAATUAL") + " - " + rs.getString("NMSISTEMA"));
		    	else
		    		sigan.setIDSISTEMAATUAL("-");
		    	
		    	sigan.setDSTIPONUMERO(rs.getString("DSTIPONUMERO"));
		    	if(rs.getString("DSTIPONUMERO") != null)
		    		sigan.setDSTIPONUMERO(rs.getString("DSTIPONUMERO"));
		    	else
		    		sigan.setDSTIPONUMERO(" - ");
		    	sigan.setDTFUTURA(rs.getString("DTFUTURA"));
		    	sigan.setDTULTIMAALTERACAO(rs.getString("DTULTIMAALTERACAO"));
		    	sigan.setLOGIN(rs.getString("LOGIN"));
		    	sigan.setCNL(rs.getString("CNL"));
		    	sigan.setDSLOCALIDADE(rs.getString("DSLOCALIDADE"));
		    	
		    	if(rs.getString("IDLOCALIDADE") != null)
		    		sigan.setIDLOCALIDADE(rs.getString("IDLOCALIDADE") + " - " + rs.getString("DSLOCALIDADE"));
		    	else
		    		sigan.setIDLOCALIDADE("-");
		    	
		    	sigan.setINNUMERO(rs.getString("INNUMERO"));
		    	sigan.setIDCATEGORIAATUAL(rs.getString("IDCATEGORIAATUAL"));
		    	
		    	if(rs.getString("IDCATEGORIAATUAL") != null)
		    		sigan.setIDCATEGORIAATUAL(rs.getString("IDCATEGORIAATUAL") + " - " + rs.getString("NMCATEGORIA"));
		    	else
		    		sigan.setIDCATEGORIAATUAL("-");
		    		
		    	sigan.setNMCATEGORIA(rs.getString("NMCATEGORIA"));
		    	sigan.setIDTPREDE(rs.getString("IDTPREDE"));
		    	if(rs.getString("IDTPREDE") != null)
		    		sigan.setIDTPREDE(rs.getString("IDTPREDE") + " - " + rs.getString("DSTPREDE"));
		    	else
		    		sigan.setIDTPREDE("-");
		    	sigan.setDSTPREDE(rs.getString("DSTPREDE"));
		    }
	    }catch (Exception e) {
			e.printStackTrace();
		} finally {
			con.close();
		}

	return sigan;	
	}

	public static ArrayList<SIGANPO> getLinhaHIST( String linha) throws SQLException{
		
		ArrayList<SIGANPO> listaSIGAN = new ArrayList(); 
		
    	Connection con = Conexao.conectarSIGAN();
	
		String sql = "SELECT H.IDMSISDN, H.DSSTATUSNUMERO, H.NMSISTEMAATUAL, H.NMSISTEMADONO, H.NMCATEGORIAATUAL, " + 
				" H.DSTIPONUMERO, H.DTULTIMAALTERACAO, H.DTALTERACAOEVENTO, H.LOGIN," + 
				" H.DSEVENTO" + 
				" FROM SIGAN_OW.HISTORICONUMERO H WHERE" + 
				" H.IDMSISDN = ? ORDER BY H.DTULTIMAALTERACAO DESC";
	 
		try {
	    	PreparedStatement ps = con.prepareStatement(sql);
		    
		    ps.setString(1, "55" + linha);
		    
		    ResultSet rs = ps.executeQuery();
		    while(rs.next()) {
		    	SIGANPO sigan = new SIGANPO();
		    	sigan.setIDMSISDN(rs.getString("IDMSISDN"));
		    	sigan.setDSSTATUS(rs.getString("DSSTATUSNUMERO"));
		    	sigan.setNMSISTEMAATUAL(rs.getString("NMSISTEMAATUAL"));
		    	sigan.setNMSISTEMADONO(rs.getString("NMSISTEMADONO"));
		    	sigan.setNMCATEGORIA(rs.getString("NMCATEGORIAATUAL"));
		    	sigan.setDSTIPONUMERO(rs.getString("DSTIPONUMERO"));
		    	sigan.setDTULTIMAALTERACAO(rs.getString("DTULTIMAALTERACAO"));
		    	sigan.setDTALTERACAOEVENTO(rs.getString("DTALTERACAOEVENTO"));
		    	sigan.setLOGIN(rs.getString("LOGIN"));
		    	sigan.setDSEVENTO(rs.getString("DSEVENTO"));
		    	listaSIGAN.add(sigan);
		    }
	    }catch (Exception e) {
			e.printStackTrace();
		} finally {
			con.close();
		}

	return listaSIGAN;	
	}

	public static ArrayList<SIGANEVENTOSPO> getLinhaEVENTOS(String linha) throws SQLException {
		ArrayList<SIGANEVENTOSPO> listaSIGANEVENTOS = new ArrayList(); 
		
    	Connection con = Conexao.conectarSIGAN();
	
		String sql = "SELECT LAE.*, ev.dsevento FROM ( " + 
				" SELECT  PREFIXONUMERO, DTTRANSACAO, IDSISTEMAORIGEM, IDSISTEMADESTINO, DSERRO, IDFUNCIONALIDADE FROM SIGAN_OW.LOGAVISOERRO  " + 
				" WHERE PREFIXONUMERO = ? " + 
				" ORDER BY DTTRANSACAO DESC " + 
				" ) LAE, SIGAN_OW.EVENTO EV WHERE SUBSTR(LAE.DSERRO, 9,2) = ev.idevento(+)";
	 
		try {
	    	PreparedStatement ps = con.prepareStatement(sql);
		    
		    ps.setString(1, "55" + linha);
		    
		    ResultSet rs = ps.executeQuery();
		    while(rs.next()) {
		    	SIGANEVENTOSPO siganeventos = new SIGANEVENTOSPO();
		    	siganeventos.setPrefixonumero(rs.getString("PREFIXONUMERO"));
		    	siganeventos.setDttransacao(rs.getString("DTTRANSACAO"));
		    	siganeventos.setIdsistemaorigem(rs.getString("IDSISTEMAORIGEM"));
		    	siganeventos.setIdsistemadestino(rs.getString("IDSISTEMADESTINO"));
		    	siganeventos.setDserro(rs.getString("DSERRO"));
		    	siganeventos.setIdfuncionalidade(rs.getString("IDFUNCIONALIDADE"));
		    	
		    	listaSIGANEVENTOS.add(siganeventos);
		    }
	    }catch (Exception e) {
			e.printStackTrace();
		} finally {
			con.close();
		}

	return listaSIGANEVENTOS;	
	}

	public static void siganMatarSessao() throws SQLException{
		
    	Connection con = Conexao.conectarSIGAN();
	
		String sql = "DELETE FROM SIGAN_ACESSO_OW.sessao WHERE idusuario = 5582"; 
		
		try{
	
		PreparedStatement ps = con.prepareStatement(sql);
	
		ps.executeQuery();
		
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			con.close();
		}
	}
	
public static ArrayList<FAIXAPO> getFaixa( String ddd, String prefixoFaixa, String sufixo) throws SQLException{
		
		
    	Connection con = Conexao.conectarSIGAN();
	
    	ArrayList<FAIXAPO> Faixas = new ArrayList<FAIXAPO>(); 
    	
		String sql = "SELECT F.IDAREAREGISTRO, F.IDPREFIXOFAIXA, F.IDMCDUINICIAL, F.MCDUFINAL, F.IDSTATUSFAIXA, SF.DSSTATUSFAIXA, O.NMOPERADORA, F.TPREDE, TN.DSTIPONUMERO, F.IDTIPONUMERO, L.IDLOCALIDADE, L.DSLOCALIDADE, L.CNL " + 
				" FROM SIGAN_OW.FAIXANUMERO F, SIGAN_OW.OPERADORA O, SIGAN_OW.TIPONUMERO TN, SIGAN_OW.STATUSFAIXA SF, SIGAN_OW.LOCALIDADE L" + 
				" WHERE" + 
				" F.IDOPERADORA = O.IDOPERADORA AND" + 
				" F.IDLOCALIDADE = L.IDLOCALIDADE AND" + 
				" F.IDTIPONUMERO = TN.IDTIPONUMERO(+) AND" + 
				" SF.IDSTATUSFAIXA = F.IDSTATUSFAIXA AND" + 
				" F.IDAREAREGISTRO = ? AND " + 
				" F.IDPREFIXOFAIXA = ? " + 
				" AND ? BETWEEN IDMCDUINICIAL AND MCDUFINAL" + 
				" ORDER BY IDMCDUINICIAL ";
	 
		try {
	    	PreparedStatement ps = con.prepareStatement(sql);
		    
		    ps.setString(1, ddd);
		    ps.setString(2, prefixoFaixa);
		    ps.setString(3, sufixo);
		    
		    
		    ResultSet rs = ps.executeQuery();
		    if(rs.next()) {
		    FAIXAPO Faixa = new FAIXAPO();
		    Faixa.setDDD(rs.getString("IDAREAREGISTRO"));
		    Faixa.setPREFIXO(rs.getString("IDPREFIXOFAIXA"));
		    Faixa.setSUFIXOINICIAL(rs.getString("IDMCDUINICIAL"));
		    Faixa.setSUFIXOFINAL(rs.getString("MCDUFINAL"));
		    Faixa.setIDSTATUSFAIXA(rs.getString("IDSTATUSFAIXA"));
		    Faixa.setSTATUSFAIXA(rs.getString("DSSTATUSFAIXA"));
		    Faixa.setOPERADORA(rs.getString("NMOPERADORA"));
		    Faixa.setTPREDE(rs.getString("TPREDE"));
		    Faixa.setTIPONUMERO(rs.getString("DSTIPONUMERO"));
		    Faixa.setIDTIPONUMERO(rs.getString("IDTIPONUMERO"));
		    Faixa.setIDLOCALIDADE(rs.getString("IDLOCALIDADE"));
		    Faixa.setDSLOCALIDADE(rs.getString("DSLOCALIDADE"));
		    Faixa.setCNL(rs.getString("CNL"));
		    Faixas.add(Faixa);
		    }
	    }catch (Exception e) {
			e.printStackTrace();
		} finally {
			con.close();
		}
		return Faixas;
}

public static ArrayList<FAIXAPO> getFaixaInteira( String ddd, String prefixoFaixa) throws SQLException{
	
	ArrayList<FAIXAPO> ListaFaixaInteira = new ArrayList<FAIXAPO>();
	
	Connection con = Conexao.conectarSIGAN();

	String sql = " SELECT F.IDAREAREGISTRO, F.IDPREFIXOFAIXA, F.IDMCDUINICIAL, F.MCDUFINAL, F.IDSTATUSFAIXA, SF.DSSTATUSFAIXA, O.NMOPERADORA, F.TPREDE, L.IDLOCALIDADE, L.DSLOCALIDADE, L.CNL, " + 
			" TN.DSTIPONUMERO, F.IDTIPONUMERO " +
			" FROM SIGAN_OW.FAIXANUMERO F, SIGAN_OW.OPERADORA O, SIGAN_OW.TIPONUMERO TN, SIGAN_OW.STATUSFAIXA SF, SIGAN_OW.LOCALIDADE L " + 
			" WHERE F.IDOPERADORA = O.IDOPERADORA AND " + 
			" F.IDTIPONUMERO = TN.IDTIPONUMERO(+) AND " + 
			" SF.IDSTATUSFAIXA = F.IDSTATUSFAIXA AND " + 
			" L.IDLOCALIDADE = F.IDLOCALIDADE AND " +
			" F.IDAREAREGISTRO = ? AND " + 
			" F.IDPREFIXOFAIXA = ? " +
			" ORDER BY IDMCDUINICIAL";
 
	try {
    	PreparedStatement ps = con.prepareStatement(sql);
	    
	    ps.setString(1, ddd);
	    ps.setString(2, prefixoFaixa);

	    ResultSet rs = ps.executeQuery();
	    while(rs.next()) {
	    FAIXAPO Faixainteira = new FAIXAPO(); 
	    Faixainteira.setDDD(rs.getString("IDAREAREGISTRO"));
	    Faixainteira.setPREFIXO(rs.getString("IDPREFIXOFAIXA"));
	    Faixainteira.setSUFIXOINICIAL(rs.getString("IDMCDUINICIAL"));
	    Faixainteira.setSUFIXOFINAL(rs.getString("MCDUFINAL"));
	    Faixainteira.setIDSTATUSFAIXA(rs.getString("IDSTATUSFAIXA"));
	    Faixainteira.setSTATUSFAIXA(rs.getString("DSSTATUSFAIXA"));
	    Faixainteira.setOPERADORA(rs.getString("NMOPERADORA"));
	    Faixainteira.setTPREDE(rs.getString("TPREDE"));
	    Faixainteira.setTIPONUMERO(rs.getString("DSTIPONUMERO"));
	    Faixainteira.setIDTIPONUMERO(rs.getString("IDTIPONUMERO"));
	    Faixainteira.setIDLOCALIDADE(rs.getString("IDLOCALIDADE"));
	    Faixainteira.setDSLOCALIDADE(rs.getString("DSLOCALIDADE"));
	    Faixainteira.setCNL(rs.getString("CNL"));
	    ListaFaixaInteira.add(Faixainteira);
	    }
    }catch (Exception e) {
		e.printStackTrace();
	} finally {
		con.close();
	}
	return ListaFaixaInteira;
}
}
