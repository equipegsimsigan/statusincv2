package br.com.indracompany.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.indracompany.process.GSIMPO;

public class HistoricoGSIMDAO {
	
	
	public static GSIMPO getLinha( String iccid) throws SQLException{
		
		
    	Connection con = Conexao.conectarGSIM();
    	GSIMPO gsimiccidhis = new GSIMPO();
	
		String sql = "select h.iccid, h.msisdn, h.IMSI, h.date_new, h.USR_NEW , h.status_code, s.STATUS_DESCRIPTION, h.ID_ICCID_PATTERN " + 
				"from gsim.simcard_hist h, GSIM.SIMCARD_STATUS s  " + 
				"where h.STATUS_CODE = s.STATUS_CODE and h.iccid in  " + 
				"( " + 
				"? " + 
				")";
	 
		try {
	    	PreparedStatement ps = con.prepareStatement(sql);
		    
			ps.setString(1, iccid);
		    
		    ResultSet rs = ps.executeQuery();
		    rs.next();
	    	gsimiccidhis.setICCID(rs.getString("ICCID"));
	    	gsimiccidhis.setMSISDN(rs.getString("MSISDN"));
	    	gsimiccidhis.setIMSI(rs.getString("IMSI"));
	    	gsimiccidhis.setDATE_NEW(rs.getString("DATE_NEW"));
	    	gsimiccidhis.setSTATUS_CODE(rs.getString("STATUS_CODE"));
	    	gsimiccidhis.setUSR_NEW(rs.getString("USR_NEW"));
	    	gsimiccidhis.setSTATUS_DESCRIPTION(rs.getString("USR_DESCRIPTION"));
	    	gsimiccidhis.setID_ICCID_PATTERN(rs.getString("ID_ICCID_PATTERN"));

	    	
	    	
		    	
	    }catch (Exception e) {
			e.printStackTrace();
		} finally {
			con.close();
		}

	return gsimiccidhis;	
	}

}
	
	

	
