package br.com.indracompany.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.indracompany.process.SPNPO;

public class BancoSPN {
	
	
	public static SPNPO getLinhaSPN( String linha) throws SQLException{
		
		
    	Connection con = Conexao.conectarSPN();
    	SPNPO spn = new SPNPO();
	
		String sql = "select NR_TELEFONE, CD_STATUS_VERSAO, DT_JANELA_MIGRACAO from SPN_TRANSACAO WHERE NR_TELEFONE in (?)";
	 
		try {
	    	PreparedStatement ps = con.prepareStatement(sql);
		    
		    ps.setString(1, linha);
		    
		    ResultSet rs = ps.executeQuery();
		    if(rs.next()) {
		    	spn.setNRTELEFONE(rs.getString("NR_TELEFONE"));
		    	spn.setSTATUSVERSAO(rs.getString("CD_STATUS_VERSAO"));
		    	spn.setDATAJANELAMIGRACAO(rs.getString("DT_JANELA_MIGRACAO"));
		    } else {
		    	System.out.println("Resultados não encontrados em SPN");
		    }
	    }catch (Exception e) {
			e.printStackTrace();
		} finally {
			con.close();
		}

	return spn;	
	}

}
	
	

	