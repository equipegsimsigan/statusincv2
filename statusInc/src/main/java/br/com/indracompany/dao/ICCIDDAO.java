package br.com.indracompany.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.indracompany.process.GSIMPO;

public class ICCIDDAO {
	
	
	public static GSIMPO getLinha( String iccid) throws SQLException{
		
		
    	Connection con = Conexao.conectarGSIM();
    	GSIMPO gsimiccid = new GSIMPO();
	
		String sql = "select sc.iccid, sc.msisdn, sc.IMSI , /*sc.ID_REMOTE_ENABLE_LOT_ORDER ,*/sc.date_new, sc.status_code, sc.USR_NEW, s.STATUS_DESCRIPTION, " + 
				" combo, sc.ID_REMOTE_ENABLE_LOT_ORDER,sc.ID_INPUT_FILE, sc.id_denatran_lot_order, sc.ID_ICCID_PATTERN " + 
				" from gsim.simcard sc, gsim.simcard_status s    " + 
				" where s.STATUS_CODE = sc.STATUS_CODE and sc.iccid IN " + 
				 " ( " + 
				" ?)";
	 
		try {
	    	PreparedStatement ps = con.prepareStatement(sql);
		    
			ps.setString(1, iccid);
		    
		    ResultSet rs = ps.executeQuery();
		    rs.next();
	    	gsimiccid.setICCID(rs.getString("ICCID"));
	    	gsimiccid.setMSISDN(rs.getString("MSISDN"));
	    	gsimiccid.setIMSI(rs.getString("IMSI"));
	    	gsimiccid.setDATE_NEW(rs.getString("DATE_NEW"));
	    	gsimiccid.setSTATUS_CODE(rs.getString("STATUS_CODE"));
	    	gsimiccid.setUSR_NEW(rs.getString("USR_NEW"));
	    	gsimiccid.setSTATUS_DESCRIPTION(rs.getString("USR_DESCRIPTION"));
	    	gsimiccid.setCOMBO(rs.getString("COMBO"));
	    	gsimiccid.setID_REMOTE_ENABLE_LOT_ORDER(rs.getString("ID_REMOTE_ENABLE_LOT_ORDER"));
	    	gsimiccid.setID_INPUT_FILE(rs.getString("ID_INPUT_FILE"));
	    	gsimiccid.setID_DENATRAN_LOT_ORDER(rs.getString("ID_DENATRAN_LOT_ORDER"));
	    	gsimiccid.setID_ICCID_PATTERN(rs.getString("ID_ICCID_PATTERN"));
	    	
	    	
		    	
	    }catch (Exception e) {
			e.printStackTrace();
		} finally {
			con.close();
		}

	return gsimiccid;	
	}

}
	
	

	