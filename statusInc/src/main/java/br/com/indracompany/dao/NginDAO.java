package br.com.indracompany.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.com.indracompany.process.NGINPO;

public class NginDAO {
	
	
	public static NGINPO getLinha( String linha) throws SQLException{
		
		
    	Connection con = Conexao.conectarNGIN();
    	NGINPO ngin = new NGINPO();
	
		String sql = " Select c.msisdn as IDMSISDN, 											  " + 
					 "    e.description as STATUS_NGIN, 									  " + 
					 "    f.tecnology as TECNOLOGIA_NGIN, 									  " + 
					 "    c.INSERT_DATE ,													  " + 
					 "    DECODE(a.CLIENT_TYPE,7,2,8,1,12,4) as TIPO_NUMERO_NGIN,			  " + 
					 "    a.DEALER															  " + 
					 "from																	  " + 
					 "    acc_private_client a												  " + 
					 "    LEFT JOIN acc_private_msisdn c ON a.partition_key = c.partition_key " + 
					 "    LEFT JOIN acc_state e ON e.CODE = a.state							  " + 
					 "    LEFT JOIN acc_profile f ON a.profile = F.Code 					  " + 
					 "where 																  " + 
					 "    a.ACCOUNT = c.ACCOUNT												  " + 
					 "    and c.msisdn in (?)												   ";  
	 
		try {
	    	PreparedStatement ps = con.prepareStatement(sql);
		    
		    ps.setString(1, linha);
		    
		    ResultSet rs = ps.executeQuery();
		    if(rs.next()) {
			    ngin.setMSISDN(rs.getString("IDMSISDN"));
		    	ngin.setSTATUS(rs.getString("STATUS_NGIN"));
		    	ngin.setTECNOLOGIA(rs.getString("TECNOLOGIA_NGIN"));
		    	ngin.setDATA(rs.getString("INSERT_DATE"));
		    	ngin.setTIPONUMERO(rs.getString("TIPO_NUMERO_NGIN"));
		    	ngin.setDEALER(rs.getString("DEALER"));
		    }
	    }catch (Exception e) {
			e.printStackTrace();
		} finally {
			con.close();
		}

	return ngin;	
	}

}
	
	

	