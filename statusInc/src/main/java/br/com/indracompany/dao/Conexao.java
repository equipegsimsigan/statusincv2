package br.com.indracompany.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class Conexao {

	public static Connection conectarSIGAN() throws SQLException{
	
		String port = "1521";
		
		//PRE PROD
//		String server = "10.129.229.113";  
//		String database = "sigandev";     
//		String user = "DBA_INDRA";
//		String passwd = "vivo2015";		
		
		//PROD
//		String server = "10.238.50.35";
//		String database = "siganpr1";
		String user = "sigan_cn";
		String passwd = "sig45#ty";        
        
//      String url = "jdbc:oracle:thin:@"+ server+":"+ port+":"+ database;
		String url = "jdbc:oracle:thin:@10.238.50.35:1525:siganpr1";      					
		
        Connection conexao = null;
        
        try {        	
        	conexao = DriverManager.getConnection(url, user, passwd); 
        }catch (Exception e) {
        	e.printStackTrace();
		}
         
        return conexao;
    }
	
	public static Connection conectarNGIN() throws SQLException{
			
		String port = "1521";
				
		//PROD
		String server = "10.128.2.177";
		String database = "denise";
		String user = "support";
		String passwd = "support";        
        
		String url = "jdbc:oracle:thin:@"+ server+":"+ port+":"+ database;
//		String url = "jdbc:oracle:thin:@10.238.50.35:1525:siganpr1";      					
		
        Connection conexao = null;
        
        try {        	
        	conexao = DriverManager.getConnection(url, user, passwd); 
        }catch (Exception e) {
        	e.printStackTrace();
		}
         
        return conexao;
    }
	
	public static Connection conectarSPN() throws SQLException{
		
		String port = "1521";
				
		//PROD
		String server = "10.238.49.65";
		String database = "nsoabpr";
		String user = "spn_ow";
		String passwd = "spn1234";        
        
		String url = "jdbc:oracle:thin:@"+ server+":"+ port+"/"+ database;
//		String url = "jdbc:oracle:thin:@10.238.50.35:1525:siganpr1";      					
		
        Connection conexao = null;
        
        try {        	
        	conexao = DriverManager.getConnection(url, user, passwd); 
        }catch (Exception e) {
        	e.printStackTrace();
		}
         
        return conexao;
    }
	
public static Connection conectarGSIM() throws SQLException{
		
		String port = "1521";
				
		//PROD
		String server = "10.238.49.32";
		String database = "simpr";
		String user = "indralog";
		String passwd = "vivo01";        
        
		String url = "jdbc:oracle:thin:@"+ server+":"+ port+"/"+ database;
//		String url = "jdbc:oracle:thin:@10.238.50.35:1525:siganpr1";      					
		
        Connection conexao = null;
        
        try {        	
        	conexao = DriverManager.getConnection(url, user, passwd); 
        }catch (Exception e) {
        	e.printStackTrace();
		}
         
        return conexao;
    }
	
	
}  
	

