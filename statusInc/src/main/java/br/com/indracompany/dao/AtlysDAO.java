package br.com.indracompany.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import br.com.indracompany.controller.ConnectionUtil;
import br.com.indracompany.process.AtlysPO;
import br.com.indracompany.process.FAIXAPO;

public class AtlysDAO {
	
	
	public static AtlysPO getLinha( String linha) throws SQLException{
		
		
    	Connection con = ConnectionUtil.getConnectionATLYS();
    	AtlysPO atlys = new AtlysPO();
	
		String sql = "SELECT ANBR.ACCESS_NBR AS IDMSISDN                ,ANBR.ACCESS_NBR_STATUS_CD AS STATUS_ATLYS                ,CASE SBS.SBSCRP_TYPE_CD                                  WHEN '01'                                               THEN '01'                               WHEN '7'                                               THEN '01'                               WHEN 'F'                                               THEN '01'                               WHEN 'T'                                               THEN '01'                               WHEN 'V'                                               THEN '03'                               WHEN '4'                                               THEN '04'                               WHEN 'O'                                               THEN '04'                               WHEN '6'                                               THEN '15'                               WHEN 'VF'                                               THEN '19'                               WHEN 'VJ'                                               THEN '19'                               WHEN 'JP'                                               THEN '22'                               "
				+ "ELSE 'NÃƒO MAPEADO - ' || SBS.SBSCRP_TYPE_CD                               END AS TIPO_NUMERO_ATLYS,            ANBR.RLSE_DT AS DATA_ATLYS FROM ACCESS_NBR ANBR                ,ACCESS_LINE_NBR_ASGM ALNA                ,SBSCRP SBS WHERE NOT EXISTS(                               SELECT 1                               FROM ACCESS_LINE_NBR_ASGM B                               WHERE B.ACCESS_NBR = ALNA.ACCESS_NBR                                               AND B.CREATE_DT > ALNA.CREATE_DT                               )                "
				+ "AND ANBR.ACCESS_NBR = ALNA.ACCESS_NBR                AND ALNA.SBSCRP_ID = SBS.SBSCRP_ID                AND SBS.SBSCRP_SVC_EFF_DT = "
				+ "(                               SELECT MAX(SBS.SBSCRP_SVC_EFF_DT)                               FROM SBSCRP A2                               WHERE A2.SBSCRP_ID = SBS.SBSCRP_ID                               )                "
				+ "AND ANBR.ACCESS_NBR IN (                                 ?)     GROUP BY ANBR.ACCESS_NBR                ,ANBR.ACCESS_NBR_STATUS_CD                ,ANBR.ACCESS_NBR_AVAIL_DT                ,ANBR.CAT_CD                ,SBS.CHNL_TYPE_IND                ,SBS.SBSCRP_TYPE_CD                ,ANBR.RLSE_DT UNION SELECT DISTINCT ALNA.ACCESS_NBR AS IDMSISDN                ,ALNA.SWITCH_STATUS_CD AS STATUS_ATLYS                ,CASE SBS.SBSCRP_TYPE_CD                                "
				+ "WHEN '01'                                               THEN '01'                               WHEN '7'                                               THEN '01'                               WHEN 'F'                                               THEN '01'                               WHEN 'T'                                               THEN '01'                               WHEN 'V'                                               THEN '03'                               WHEN '4'                                               THEN '04'                               WHEN 'O'                                               THEN '04'                               WHEN '6'                                               THEN '15'                               WHEN 'VF'                                               THEN '19'                               WHEN 'VJ'                                               THEN '19'                               WHEN 'JP'                                               THEN '22'                               ELSE 'NÃƒO MAPEADO - ' || SBS.SBSCRP_TYPE_CD                               END AS TIPO_NUMERO_ATLYS,          ACCESS_NBR_EFF_DT AS DATA_ATLYS FROM ACCESS_LINE_NBR_ASGM ALNA LEFT JOIN SBSCRP SBS ON ALNA.SBSCRP_ID = SBS.SBSCRP_ID WHERE ALNA.ACCESS_NBR_EFF_DT || ALNA.ACCESS_NBR_EFF_TM = (                               SELECT MAX(A1.ACCESS_NBR_EFF_DT || A1.ACCESS_NBR_EFF_TM)                               FROM ACCESS_LINE_NBR_ASGM A1                               WHERE A1.ACCESS_NBR = ALNA.ACCESS_NBR                               )                AND NOT EXISTS (                               SELECT 1                               FROM ACCESS_NBR ANBR                               WHERE ANBR.ACCESS_NBR = ALNA.ACCESS_NBR                               )                AND ALNA.ACCESS_NBR IN (                                ?)";
	    try {
	    	PreparedStatement ps = con.prepareStatement(sql);
		    
		    ps.setString(1, linha);
		    ps.setString(2, linha);
		    
		    ResultSet rs = ps.executeQuery();
		    if(rs.next()) {
		    	atlys.setMSISDN(rs.getString("IDMSISDN"));
		    	atlys.setSTATUS(rs.getString("STATUS_ATLYS"));
		    	atlys.setTIPONUMERO(rs.getString("TIPO_NUMERO_ATLYS"));
		    	atlys.setDATAATLYS(rs.getString("DATA_ATLYS"));
		    } else {
		    	System.out.println("Resultado não encontrado no ATLYS");
		    }	
	    }catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionUtil.closeConnection(con);
		}

	return atlys;	
	}


public static ArrayList<FAIXAPO> getFaixaAtlys(String ddd, String prefixo) throws SQLException{
		
		
    	Connection con = ConnectionUtil.getConnectionATLYS();
    	
	
    	ArrayList<FAIXAPO> faixas = new ArrayList<FAIXAPO>();
    	
		String sql = " SELECT RATE_CENTER_ID, BEGIN_ACCESS_NBR, END_ACCESS_NBR, NTWRK_NBR_TYPE_CD  " + 
				     " FROM access_line_nbr_range@prodlk WHERE begin_access_nbr LIKE ? ORDER BY begin_access_nbr "; 
	    try {
	    	PreparedStatement ps = con.prepareStatement(sql);
		    
		    ps.setString(1, ddd + prefixo + '%');
		    
		    ResultSet rs = ps.executeQuery();
		    	if(rs.next()) {
		    	FAIXAPO faixaAtlys = new FAIXAPO();
		    	faixaAtlys.setRATE_CENTER_ID(rs.getString("RATE_CENTER_ID"));
		    	faixaAtlys.setBEGIN_ACESS_NBR(rs.getString("BEGIN_ACCESS_NBR"));
		    	faixaAtlys.setEND_ACESS_NBR(rs.getString("END_ACCESS_NBR"));
		    	faixaAtlys.setNTWRK_NBR_TYPE_CD(rs.getString("NTWRK_NBR_TYPE_CD"));
		    	faixas.add(faixaAtlys);
//		    } else {
//		    	System.out.println("Faixa não cadastrada no ATLYS");
		    }	
	    }catch (Exception e) {
			e.printStackTrace();
		} finally {
			ConnectionUtil.closeConnection(con);
		}

	return faixas;	
	}

public static ArrayList<FAIXAPO> getFaixaInteiraAtlys(String ddd, String prefixo) throws SQLException{
	
	
	Connection con = ConnectionUtil.getConnectionATLYS();
	

	ArrayList<FAIXAPO> faixaInteiraAtlys = new ArrayList();
	
	String sql = " SELECT RATE_CENTER_ID, BEGIN_ACCESS_NBR, END_ACCESS_NBR, NTWRK_NBR_TYPE_CD  " + 
			     " FROM access_line_nbr_range@prodlk WHERE begin_access_nbr LIKE ? ORDER BY begin_access_nbr "; 
    try {
    	PreparedStatement ps = con.prepareStatement(sql);
	    
	    ps.setString(1, ddd + prefixo + '%');
	    
	    ResultSet rs = ps.executeQuery();
	    while(rs.next()) {
	    	FAIXAPO faixaAtlys = new FAIXAPO();
	    	faixaAtlys.setRATE_CENTER_ID(rs.getString("RATE_CENTER_ID"));
	    	faixaAtlys.setBEGIN_ACESS_NBR(rs.getString("BEGIN_ACCESS_NBR"));
	    	faixaAtlys.setEND_ACESS_NBR(rs.getString("END_ACCESS_NBR"));
	    	faixaAtlys.setNTWRK_NBR_TYPE_CD(rs.getString("NTWRK_NBR_TYPE_CD"));
	    	faixaInteiraAtlys.add(faixaAtlys);
//	    } else {
//	    	System.out.println("Faixa não cadastrada no ATLYS");
	    }	
    }catch (Exception e) {
		e.printStackTrace();
	} finally {
		ConnectionUtil.closeConnection(con);
	}

    return faixaInteiraAtlys;	
	}
	
	}
