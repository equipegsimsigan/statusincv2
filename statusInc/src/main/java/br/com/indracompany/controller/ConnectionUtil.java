package br.com.indracompany.controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException; 

public class ConnectionUtil {
	
	private static Connection dbConnection;
	
	public static final String DB_DRIVER 	 = "oracle.jdbc.driver.OracleDriver";
	
	//Conexão com o banco do SIGAN
	public static final String DB_HOST_SIGAN   	   = "10.238.50.35";
	public static final String DB_PORT_SIGAN   	   = "1525";
	public static final String DB_NAME_SIGAN   	   = "siganpr";
	public static final String USR_LOGIN_SIGAN 	   = "sigan_cn";
	public static final String USR_PASSW_SIGAN 	   = "sig45#ty";
	public static final String DB_CONNECTION_SIGAN = "jdbc:oracle:thin:@" + DB_HOST_SIGAN + ":" + DB_PORT_SIGAN + "/" + DB_NAME_SIGAN;
	
	//Conexão com o banco do ATLYS
	public static final String DB_HOST_ATLYS       = "10.238.47.71";
	public static final String DB_PORT_ATLYS       = "1521";
	public static final String DB_NAME_ATLYS       = "srv_geral.world";
	public static final String USR_LOGIN_ATLYS     = "db_dev";
	public static final String USR_PASSW_ATLYS     = "db_dev";
	public static final String DB_CONNECTION_ATLYS = "jdbc:oracle:thin:@" + DB_HOST_ATLYS + ":" + DB_PORT_ATLYS + "/" + DB_NAME_ATLYS;
	
	//Conexão com o banco do NGIN
	public static final String DB_HOST_NGIN = "10.128.2.177";
	public static final String DB_PORT_NGIN = "1521";
	public static final String DB_NAME_NGIN = "denise";
	public static final String USR_LOGIN_NGIN = "support";
	public static final String USR_PASSW_NGIN = "support";
	public static final String DB_CONNECTION_NGIN = "jdbc:oracle:thin:@" + DB_HOST_NGIN + ":"  + DB_PORT_NGIN + "/" + DB_NAME_NGIN;

	//Conexão com o banco da SPN
	public static final String DB_HOST_SPN = "10.238.49.65";
	public static final String DB_PORT_SPN = "1521";
	public static final String DB_NAME_SPN = "nsoabpr";
	public static final String USR_LOGIN_SPN = "spn_ow";
	public static final String USR_PASSW_SPN = "spn1234";
	public static final String DB_CONNECTION_SPN = "jdbc:oracle:thin:@" + DB_HOST_SPN + ":"  + DB_PORT_SPN + "/" + DB_NAME_SPN;
	
	//Conexão com o banco do GSIM
	public static final String DB_HOST_GSIM = "10.238.49.32";
	public static final String DB_PORT_GSIM = "1521";
	public static final String DB_NAME_GSIM = "simpr";
	public static final String USR_LOGIN_GSIM = "indralog";
	public static final String USR_PASSW_GSIM = "vivo01";
	public static final String DB_CONNECTION_GSIM = "jdbc:oracle:thin:@" + DB_HOST_GSIM + ":"  + DB_PORT_GSIM + "/" + DB_NAME_GSIM;
	
	
	public static Connection getConnectionSIGAN() {
		
		try{  
			Class.forName(DB_DRIVER);
			try {
				dbConnection = DriverManager.getConnection(DB_CONNECTION_SIGAN, USR_LOGIN_SIGAN, USR_PASSW_SIGAN);
			} catch(SQLException se) {System.out.println(se.getMessage());}
		} catch(ClassNotFoundException cnfe) {System.out.println(cnfe.getMessage());}
		
		return dbConnection;
	}
	
	
	public static Connection getConnectionATLYS() {
		
		try{  
			Class.forName(DB_DRIVER);
			try {
				dbConnection = DriverManager.getConnection(DB_CONNECTION_ATLYS, USR_LOGIN_ATLYS, USR_PASSW_ATLYS);
			} catch(SQLException se) {System.out.println(se.getMessage());}
		} catch(ClassNotFoundException cnfe) {System.out.println(cnfe.getMessage());}
		
		return dbConnection;
	}
	
	public static Connection getConnectionNGIN() {
		
		try{  
			Class.forName(DB_DRIVER);
			try {
				dbConnection = DriverManager.getConnection(DB_CONNECTION_NGIN, USR_LOGIN_NGIN, USR_PASSW_NGIN);
			} catch(SQLException se) {System.out.println(se.getMessage());}
		} catch(ClassNotFoundException cnfe) {System.out.println(cnfe.getMessage());}
		
		return dbConnection;
	}
	
	public static Connection getConnectionSPN() {
		
		try{  
			Class.forName(DB_DRIVER);
			try {
				dbConnection = DriverManager.getConnection(DB_CONNECTION_SPN, USR_LOGIN_SPN, USR_PASSW_SPN);
			} catch(SQLException se) {System.out.println(se.getMessage());}
		} catch(ClassNotFoundException cnfe) {System.out.println(cnfe.getMessage());}
		
		return dbConnection;
	}
	
	public static Connection getConnectionGSIM() {
		
		try{  
			Class.forName(DB_DRIVER);
			try {
				dbConnection = DriverManager.getConnection(DB_CONNECTION_GSIM, USR_LOGIN_GSIM, USR_PASSW_GSIM);
			} catch(SQLException se) {System.out.println(se.getMessage());}
		} catch(ClassNotFoundException cnfe) {System.out.println(cnfe.getMessage());}
		
		return dbConnection;
	}
		
	
	
	public static void closeConnection(Connection con) {
		try {
			con.close();
		} catch(SQLException se) {System.out.println(se.getMessage());}
	}
	
	
	
	
}

