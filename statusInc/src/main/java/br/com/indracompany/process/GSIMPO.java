package br.com.indracompany.process;

public class GSIMPO {
	
	private String ICCID;
	private String MSISDN;
	private String IMSI;
	private String STATUS_CODE;
	private String STATUS_DESCRIPTION;
	private String DATE_NEW;
	private String USR_NEW;
	private String STATUS_DESCRIPTION2;
	private String COMBO;
	private String ID_REMOTE_ENABLE_LOT_ORDER;
	private String ID_INPUT_FILE;
	private String ID_DENATRAN_LOT_ORDER;
	private String ID_ICCID_PATTERN;
	private String TIPO_SIMCARD;
	private String ID_BLACK_LIST;
	private String STATUS;
	private String USR_UPDATE;
	private String DATE_UPDATE;
	
	public GSIMPO() {
		this.ICCID = "";
		this.MSISDN = "";
		this.IMSI = "";
		this.STATUS_CODE = "";
		this.STATUS_DESCRIPTION = "";
		this.DATE_NEW = "";
		this.USR_NEW = "";
		this.STATUS_DESCRIPTION2 = "";
		this.COMBO = "";
		this.ID_REMOTE_ENABLE_LOT_ORDER = "";
		this.ID_INPUT_FILE = "";
		this.ID_DENATRAN_LOT_ORDER = "";
		this.ID_ICCID_PATTERN = "";
		this.TIPO_SIMCARD = "";
		this.ID_BLACK_LIST = "";
		this.STATUS = "";
		this.USR_UPDATE = "";
		this.DATE_UPDATE = "";
	}
	
	public String getID_BLACK_LIST() {
		return ID_BLACK_LIST;
	}

	public void setID_BLACK_LIST(String iD_BLACK_LIST) {
		ID_BLACK_LIST = iD_BLACK_LIST;
	}

	public String getSTATUS() {
		return STATUS;
	}


	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}


	public String getUSR_UPDATE() {
		return USR_UPDATE;
	}


	public void setUSR_UPDATE(String uSR_UPDATE) {
		USR_UPDATE = uSR_UPDATE;
	}


	public String getDATE_UPDATE() {
		return DATE_UPDATE;
	}

	public void setDATE_UPDATE(String DATE_UPDATE) {
		this.DATE_UPDATE = DATE_UPDATE.substring(8, 10) + "/" + DATE_UPDATE.substring(5,7) + "/" + DATE_UPDATE.substring(0,4) +" "+ DATE_UPDATE.substring(11,13) +":"+ DATE_UPDATE.substring(14,15) + DATE_UPDATE.substring(15,16) +":"+ DATE_UPDATE.substring(17,19);
	}

	public String getTIPO_SIMCARD() {
		return TIPO_SIMCARD;
	}

	public void setTIPO_SIMCARD(String tIPO_SIMCARD) {
		TIPO_SIMCARD = tIPO_SIMCARD;
	}


	public String getICCID() {
		return ICCID;
	}

	public void setICCID(String iCCID) {
		ICCID = iCCID;
	}



	public String getMSISDN() {
		return MSISDN;
	}



	public void setMSISDN(String mSISDN) {
		MSISDN = mSISDN;
	}



	public String getIMSI() {
		return IMSI;
	}



	public void setIMSI(String iMSI) {
		IMSI = iMSI;
	}



	public String getSTATUSCODE() {
		return STATUS_CODE;
	}



	public void setSTATUSCODE(String sTATUSCODE) {
		STATUS_CODE = sTATUSCODE;
	}



	public String getSTATUS_DESCRIPTION() {
		return STATUS_DESCRIPTION;
	}



	public void setSTATUS_DESCRIPTION(String sTATUS_DESCRIPTION) {
		STATUS_DESCRIPTION = sTATUS_DESCRIPTION;
	}



	public String getDATE_NEW() {
		return DATE_NEW;
	}



	public void setDATE_NEW(String DATE_NEW) {
		this.DATE_NEW = DATE_NEW.substring(8, 10) + "/" + DATE_NEW.substring(5,7) + "/" + DATE_NEW.substring(0,4) +" "+ DATE_NEW.substring(11,13) +":"+ DATE_NEW.substring(14,15) + DATE_NEW.substring(15,16) +":"+ DATE_NEW.substring(17,19);
	}



	public String getUSR_NEW() {
		return USR_NEW;
	}



	public void setUSR_NEW(String uSR_NEW) {
		USR_NEW = uSR_NEW;
	}



	public String getSTATUS_CODE() {
		return STATUS_CODE;
	}



	public void setSTATUS_CODE(String sTATUS_CODE) {
		STATUS_CODE = sTATUS_CODE;
	}




	public String getSTATUS_DESCRIPTION2() {
		return STATUS_DESCRIPTION2;
	}



	public void setSTATUS_DESCRIPTION2(String sTATUS_DESCRIPTION2) {
		STATUS_DESCRIPTION2 = sTATUS_DESCRIPTION2;
	}



	public String getCOMBO() {
		return COMBO;
	}



	public void setCOMBO(String cOMBO) {
		COMBO = cOMBO;
	}



	public String getID_REMOTE_ENABLE_LOT_ORDER() {
		return ID_REMOTE_ENABLE_LOT_ORDER;
	}



	public void setID_REMOTE_ENABLE_LOT_ORDER(String iD_REMOTE_ENABLE_LOT_ORDER) {
		ID_REMOTE_ENABLE_LOT_ORDER = iD_REMOTE_ENABLE_LOT_ORDER;
	}



	public String getID_INPUT_FILE() {
		return ID_INPUT_FILE;
	}



	public void setID_INPUT_FILE(String iD_INPUT_FILE) {
		ID_INPUT_FILE = iD_INPUT_FILE;
	}



	public String getID_DENATRAN_LOT_ORDER() {
		return ID_DENATRAN_LOT_ORDER;
	}



	public void setID_DENATRAN_LOT_ORDER(String iD_DENATRAN_LOT_ORDER) {
		ID_DENATRAN_LOT_ORDER = iD_DENATRAN_LOT_ORDER;
	}



	public String getID_ICCID_PATTERN() {
		return ID_ICCID_PATTERN;
	}



	public void setID_ICCID_PATTERN(String iD_ICCID_PATTERN) {
		ID_ICCID_PATTERN = iD_ICCID_PATTERN;
	}

	
	
}