package br.com.indracompany.process;

public class SPNPO {
	
	private String NRTELEFONE;
	private String STATUSVERSAO;
	private String DATAJANELAMIGRACAO;


	public SPNPO() {
		this.NRTELEFONE = "";
		this.STATUSVERSAO = "";
		this.DATAJANELAMIGRACAO = "";
		
	}

	public String getNRTELEFONE() {
		return NRTELEFONE;
	}





	public void setNRTELEFONE(String nRTELEFONE) {
		this.NRTELEFONE = nRTELEFONE;
	}





	public String getSTATUSVERSAO() {
		return STATUSVERSAO;
	}





	public void setSTATUSVERSAO(String sTATUSVERSAO) {
		this.STATUSVERSAO = sTATUSVERSAO;
	}





	public String getDATAJANELAMIGRACAO() {
		return DATAJANELAMIGRACAO;
	}





	public void setDATAJANELAMIGRACAO(String DATAJANELAMIGRACAO) {
		this.DATAJANELAMIGRACAO = DATAJANELAMIGRACAO.substring(8, 10) + "/" + DATAJANELAMIGRACAO.substring(5,7) + "/" + DATAJANELAMIGRACAO.substring(0,4) +" "+ DATAJANELAMIGRACAO.substring(11,13) +":"+ DATAJANELAMIGRACAO.substring(14,15) + DATAJANELAMIGRACAO.substring(15,16) +":"+ DATAJANELAMIGRACAO.substring(17,19);
	}
	
	
}
