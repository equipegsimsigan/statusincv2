package br.com.indracompany.process;


import java.io.Console;
import java.text.SimpleDateFormat;

public class NGINPO {
	
	private String MSISDN;
	private String STATUS;
	private String TECNOLOGIA;
	private String DATA;
	private String TIPONUMERO;
	private String DEALER;

	public NGINPO() {
		this.MSISDN = "";
		this.STATUS = "";
		this.TECNOLOGIA = "";
		this.DATA = "";
		this.TIPONUMERO = "";
		this.DEALER = "";
		
	}
	

	public String getMSISDN() {
		return MSISDN;
	}


	public void setMSISDN(String mSISDN) {
		MSISDN = mSISDN;
	}


	public String getSTATUS() {
		return STATUS;
	}


	public void setSTATUS(String sTATUS) {
		STATUS = sTATUS;
	}


	public String getTECNOLOGIA() {
		return TECNOLOGIA;
	}


	public void setTECNOLOGIA(String tECNOLOGIA) {
		TECNOLOGIA = tECNOLOGIA;
	}


	public String getDATA() {
		return DATA;
	}

	public String getTIPONUMERO() {
		return TIPONUMERO;
	}


	public void setTIPONUMERO(String tIPONUMERO) {
		TIPONUMERO = tIPONUMERO;
	}


	public String getDEALER() {
		return DEALER;
	}


	public void setDEALER(String dEALER) {
		DEALER = dEALER;
	}


	public void setDATA(String DATA) { 
		System.out.println(DATA);
		this.DATA = DATA.substring(8, 10) + "/" + DATA.substring(5,7) + "/" + DATA.substring(0,4) +" "+ DATA.substring(11,13) +":"+ DATA.substring(14,15) + DATA.substring(15,16) +":"+ DATA.substring(17,19);	
	}
}