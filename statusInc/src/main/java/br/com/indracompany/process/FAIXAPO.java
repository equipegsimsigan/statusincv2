package br.com.indracompany.process;

public class FAIXAPO {

	private String DDD;
	private String PREFIXO;
	private String SUFIXOINICIAL;
	private String SUFIXOFINAL;
	private String IDSTATUSFAIXA;
	private String STATUSFAIXA;
	private String OPERADORA;
	private String TPREDE;
	private String TIPONUMERO;
	private String IDTIPONUMERO;
	private String IDLOCALIDADE;
	private String DSLOCALIDADE;
	private String CNL;
	private String RATE_CENTER_ID;
	private String BEGIN_ACESS_NBR;
	private String END_ACESS_NBR;
	private String NTWRK_NBR_TYPE_CD;
	
		
	


	public FAIXAPO() {
		this.DDD = "";
		this.PREFIXO = "";
		this.SUFIXOINICIAL = "";
		this.SUFIXOFINAL = "";
		this.IDSTATUSFAIXA = "";
		this.STATUSFAIXA = "";
		this.OPERADORA = "";
		this.TPREDE  = "";
		this.TIPONUMERO = "";
		this.IDTIPONUMERO = "";
		this.IDLOCALIDADE = "";
		this.DSLOCALIDADE = "";
		this.CNL = "";
		this.RATE_CENTER_ID = "";
		this.BEGIN_ACESS_NBR = "";
		this.END_ACESS_NBR = "";
		this.NTWRK_NBR_TYPE_CD = "";
	
	}

	public String getIDLOCALIDADE() {
		return IDLOCALIDADE;
	}


	public String getRATE_CENTER_ID() {
		return RATE_CENTER_ID;
	}

	public void setRATE_CENTER_ID(String rATE_CENTER_ID) {
		RATE_CENTER_ID = rATE_CENTER_ID;
	}

	public String getBEGIN_ACESS_NBR() {
		return BEGIN_ACESS_NBR;
	}

	public void setBEGIN_ACESS_NBR(String bEGIN_ACESS_NBR) {
		BEGIN_ACESS_NBR = bEGIN_ACESS_NBR;
	}

	public String getEND_ACESS_NBR() {
		return END_ACESS_NBR;
	}

	public void setEND_ACESS_NBR(String eND_ACESS_NBR) {
		END_ACESS_NBR = eND_ACESS_NBR;
	}

	public String getNTWRK_NBR_TYPE_CD() {
		return NTWRK_NBR_TYPE_CD;
	}

	public void setNTWRK_NBR_TYPE_CD(String nTWRK_NBR_TYPE_CD) {
		NTWRK_NBR_TYPE_CD = nTWRK_NBR_TYPE_CD;
	}

	public void setIDLOCALIDADE(String iDLOCALIDADE) {
		IDLOCALIDADE = iDLOCALIDADE;
	}


	public String getDSLOCALIDADE() {
		return DSLOCALIDADE;
	}


	public void setDSLOCALIDADE(String dSLOCALIDADE) {
		DSLOCALIDADE = dSLOCALIDADE;
	}


	public String getCNL() {
		return CNL;
	}


	public void setCNL(String cNL) {
		CNL = cNL;
	}


	public String getIDTIPONUMERO() {
		return IDTIPONUMERO;
	}


	public void setIDTIPONUMERO(String iDTIPONUMERO) {
		IDTIPONUMERO = iDTIPONUMERO;
	}
	
	public String getDDD() {
		return DDD;
	}


	public void setDDD(String dDD) {
		DDD = dDD;
	}


	public String getPREFIXO() {
		return PREFIXO;
	}


	public void setPREFIXO(String pREFIXO) {
		PREFIXO = pREFIXO;
	}


	public String getSUFIXOINICIAL() {
		return SUFIXOINICIAL;
	}


	public void setSUFIXOINICIAL(String sUFIXOINICIAL) {
		SUFIXOINICIAL = sUFIXOINICIAL;
	}


	public String getSUFIXOFINAL() {
		return SUFIXOFINAL;
	}


	public void setSUFIXOFINAL(String sUFIXOFINAL) {
		SUFIXOFINAL = sUFIXOFINAL;
	}


	public String getIDSTATUSFAIXA() {
		return IDSTATUSFAIXA;
	}


	public void setIDSTATUSFAIXA(String iDSTATUSFAIXA) {
		IDSTATUSFAIXA = iDSTATUSFAIXA;
	}


	public String getSTATUSFAIXA() {
		return STATUSFAIXA;
	}


	public void setSTATUSFAIXA(String sTATUSFAIXA) {
		STATUSFAIXA = sTATUSFAIXA;
	}


	public String getOPERADORA() {
		return OPERADORA;
	}


	public void setOPERADORA(String oPERADORA) {
		OPERADORA = oPERADORA;
	}


	public String getTPREDE() {
		return TPREDE;
	}


	public void setTPREDE(String tPREDE) {
		TPREDE = tPREDE;
	}


	public String getTIPONUMERO() {
		return TIPONUMERO;
	}


	public void setTIPONUMERO(String tIPONUMERO) {
		TIPONUMERO = tIPONUMERO;
	}

	
	
	
}

