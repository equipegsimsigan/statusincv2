package br.com.indracompany.process;

import java.util.ArrayList;

public class ResultadosPO {

	private boolean simcard;
	private NGINPO ngin;
	private AtlysPO atlys;
	private ArrayList <GSIMPO> gsimhis;
	private SPNPO spn;
	private GSIMPO gsimiccid;
	private GSIMPO gsimmsisdn;
	private GSIMPO gsimblacklist;
	private SIGANPO sigan;
	private ArrayList <SIGANPO> siganhist;
	private ArrayList <SIGANEVENTOSPO> siganeventos;
		
	
	public ResultadosPO() {
		this.ngin         = new NGINPO();
		this.atlys 		  = new AtlysPO();
		this.spn		  = new SPNPO();
		this.gsimhis      = new ArrayList<GSIMPO>();
		this.gsimiccid    = new GSIMPO();
		this.gsimmsisdn   = new GSIMPO();
		this.gsimblacklist= new GSIMPO();
		this.siganhist	  = new ArrayList<SIGANPO>();
		this.sigan        = new SIGANPO();
		this.siganeventos = new ArrayList<SIGANEVENTOSPO>();
		this.simcard      = false;
	}
	

	public GSIMPO getGsimblacklist() {
		return gsimblacklist;
	}





	public void setGsimblacklist(GSIMPO gsimblacklist) {
		this.gsimblacklist = gsimblacklist;
	}





	public boolean isSimcard() {
		return simcard;
	}





	public void setSimcard(boolean simcard) {
		this.simcard = simcard;
	}





	public ArrayList<GSIMPO> getGsimhis() {
		return gsimhis;
	}





	public void setGsimhis(ArrayList<GSIMPO> gsimhis) {
		this.gsimhis = gsimhis;
	}





	public GSIMPO getGsimiccid() {
		return gsimiccid;
	}





	public void setGsimiccid(GSIMPO gsimiccid) {
		this.gsimiccid = gsimiccid;
	}





	public GSIMPO getGsimmsisdn() {
		return gsimmsisdn;
	}





	public void setGsimmsisdn(GSIMPO gsimmsisdn) {
		this.gsimmsisdn = gsimmsisdn;
	}





	
	public NGINPO getNgin() {
		return ngin;
	}
	public void setNgin(NGINPO ngin) {
		this.ngin = ngin;
	}
	public AtlysPO getAtlys() {
		return atlys;
	}
	public void setAtlys(AtlysPO atlys) {
		this.atlys = atlys;
	}
	public SPNPO getSpn() {
		return spn;
	}
	public void setSpn(SPNPO spn) {
		this.spn = spn;
	}

	public ArrayList<SIGANEVENTOSPO> getSiganeventos() {
		return siganeventos;
	}
	public void setSiganeventos(ArrayList<SIGANEVENTOSPO> siganeventos) {
		this.siganeventos = siganeventos;
	}





	public SIGANPO getSigan() {
		return sigan;
	}





	public void setSigan(SIGANPO sigan) {
		this.sigan = sigan;
	}





	public ArrayList <SIGANPO> getSiganhist() {
		return siganhist;
	}





	public void setSiganhist(ArrayList <SIGANPO> siganhist) {
		this.siganhist = siganhist;
	}
	
}
