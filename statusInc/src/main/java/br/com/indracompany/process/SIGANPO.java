package br.com.indracompany.process;


public class SIGANPO {
	
	private String IDMSISDN;
	private String IDSTATUS;
	private String DSSTATUS;
	private String NMSISTEMA;
	private String DSTIPONUMERO;
	private String CNL;
	private String IDLOCALIDADE;
	private String IDCATEGORIAATUAL;
	private String IDTPREDE;
	private String IDTIPONUMERO;
	private String DTFUTURA;
	private String DTULTIMAALTERACAO;
	private String LOGIN;
	private String IDSISTEMAATUAL;
	private String INNUMERO;
	private String NMCATEGORIA;
	private String NMSISTEMAATUAL;
	private String NMSISTEMADONO;
	private String DTALTERACAOEVENTO;
	private String DSTPREDE;
	private String DSEVENTO;
	private String PREFIXONUMERO;
	private String DTTRANSACAO;
	private String IDSISTEMAORIGEM;
	private String IDSISTEMADESTINO;
	private String DSERRO;
	private String IDFUNCIONALIDADE;
	private String DSLOCALIDADE;

	public SIGANPO() {
		this.DSTIPONUMERO = "";
		this.CNL = "";
		this.IDMSISDN = "";
		this.IDSTATUS = "";
		this.IDLOCALIDADE = "";
		this.IDCATEGORIAATUAL = "";
		this.IDTPREDE = "";
		this.DSSTATUS = "";
		this.IDTIPONUMERO = "";
		this.DTFUTURA = "";
		this.DTULTIMAALTERACAO = "";
		this.LOGIN = "";
		this.IDSISTEMAATUAL = "";
		this.INNUMERO = "";
		this.NMCATEGORIA = "";
		this.NMSISTEMAATUAL = "";
		this.NMSISTEMADONO = "";
		this.DTALTERACAOEVENTO = "";
		this.DSTPREDE = "";
		this.DSEVENTO = "";
		this.PREFIXONUMERO = "";  
		this.DTTRANSACAO = "";        
		this.IDSISTEMAORIGEM = "";  
		this.IDSISTEMADESTINO = "";  
		this.DSERRO = "";  
		this.IDFUNCIONALIDADE = "";   
		this.DSLOCALIDADE = "";
		
	
}

	
	
	
	public String getDSLOCALIDADE() {
		return DSLOCALIDADE;
	}




	public void setDSLOCALIDADE(String dSLOCALIDADE) {
		DSLOCALIDADE = dSLOCALIDADE;
	}




	public String getNMSISTEMA() {
		return NMSISTEMA;
	}

	public void setNMSISTEMA(String nMSISTEMA) {
		NMSISTEMA = nMSISTEMA;
	}

	public String getDSTIPONUMERO() {
		return DSTIPONUMERO;
	}

	public void setDSTIPONUMERO(String dSTIPONUMERO) {
		DSTIPONUMERO = dSTIPONUMERO;
	}

	public String getCNL() {
		return CNL;
	}

	public void setCNL(String cNL) {
		CNL = cNL;
	}

	public String getIDLOCALIDADE() {
		return IDLOCALIDADE;
	}

	public void setIDLOCALIDADE(String iDLOCALIDADE) {
		IDLOCALIDADE = iDLOCALIDADE;
	}
	public String getIDCATEGORIAATUAL() {
		return IDCATEGORIAATUAL;
	}
	public void setIDCATEGORIAATUAL(String iDCATEGORIAATUAL) {
		IDCATEGORIAATUAL = iDCATEGORIAATUAL;
	}
	public String getIDTPREDE() {
		return IDTPREDE;
	}
	public void setIDTPREDE(String iDTPREDE) {
		IDTPREDE = iDTPREDE;
	}
	public String getIDMSISDN() {
		return IDMSISDN;
	}

	public void setIDMSISDN(String iDMSISDN) {
		IDMSISDN = iDMSISDN;
	}

	public String getPREFIXONUMERO() {
		return PREFIXONUMERO;
	}

	public void setPREFIXONUMERO(String pREFIXONUMERO) {
		PREFIXONUMERO = pREFIXONUMERO;
	}

	public String getDTTRANSACAO() {
		return DTTRANSACAO;
	}

	public void setDTTRANSACAO(String dTTRANSACAO) {
		DTTRANSACAO = dTTRANSACAO;
	}

	public String getIDSISTEMAORIGEM() {
		return IDSISTEMAORIGEM;
	}

	public void setIDSISTEMAORIGEM(String iDSISTEMAORIGEM) {
		IDSISTEMAORIGEM = iDSISTEMAORIGEM;
	}

	public String getIDSISTEMADESTINO() {
		return IDSISTEMADESTINO;
	}

	public void setIDSISTEMADESTINO(String iDSISTEMADESTINO) {
		IDSISTEMADESTINO = iDSISTEMADESTINO;
	}

	public String getDSERRO() {
		return DSERRO;
	}

	public void setDSERRO(String dSERRO) {
		DSERRO = dSERRO;
	}

	public String getIDFUNCIONALIDADE() {
		return IDFUNCIONALIDADE;
	}

	public void setIDFUNCIONALIDADE(String iDFUNCIONALIDADE) {
		IDFUNCIONALIDADE = iDFUNCIONALIDADE;
	}

	public String getIDSTATUS() {
		return IDSTATUS;
	}

	public void setIDSTATUS(String iDSTATUS) {
		IDSTATUS = iDSTATUS;
	}

	public String getDSSTATUS() {
		return DSSTATUS;
	}

	public void setDSSTATUS(String dSSTATUS) {
		DSSTATUS = dSSTATUS;
	}

	public String getIDTIPONUMERO() {
		return IDTIPONUMERO;
	}

	public void setIDTIPONUMERO(String iDTIPONUMERO) {
		IDTIPONUMERO = iDTIPONUMERO;
	}

	public String getDTFUTURA() {
		return DTFUTURA;
	}

	public void setDTFUTURA(String DTFUTURA) {
		this.DTFUTURA = DTFUTURA.substring(8, 10) + "/" + DTFUTURA.substring(5,7) + "/" + DTFUTURA.substring(0,4) +" "+ DTFUTURA.substring(11,13) +":"+ DTFUTURA.substring(14,15) + DTFUTURA.substring(15,16) +":"+ DTFUTURA.substring(17,19);
	}

	public String getDTULTIMAALTERACAO() {
		return DTULTIMAALTERACAO;
	}

	public void setDTULTIMAALTERACAO(String DTULTIMAALTERACAO) {
		this.DTULTIMAALTERACAO = DTULTIMAALTERACAO.substring(8, 10) + "/" + DTULTIMAALTERACAO.substring(5,7) + "/" + DTULTIMAALTERACAO.substring(0,4) +" "+ DTULTIMAALTERACAO.substring(11,13) +":"+ DTULTIMAALTERACAO.substring(14,15) + DTULTIMAALTERACAO.substring(15,16) +":"+ DTULTIMAALTERACAO.substring(17,19);
	}

	public String getLOGIN() {
		return LOGIN;
	}

	public void setLOGIN(String lOGIN) {
		LOGIN = lOGIN;
	}

	public String getIDSISTEMAATUAL() {
		return IDSISTEMAATUAL;
	}

	public void setIDSISTEMAATUAL(String iDSISTEMAATUAL) {
		IDSISTEMAATUAL = iDSISTEMAATUAL;
	}

	public String getINNUMERO() {
		return INNUMERO;
	}

	public void setINNUMERO(String iNNUMERO) {
		INNUMERO = iNNUMERO;
	}

	public String getNMCATEGORIA() {
		return NMCATEGORIA;
	}

	public void setNMCATEGORIA(String nMCATEGORIA) {
		NMCATEGORIA = nMCATEGORIA;
	}

	public String getNMSISTEMAATUAL() {
		return NMSISTEMAATUAL;
	}

	public void setNMSISTEMAATUAL(String nMSISTEMAATUAL) {
		NMSISTEMAATUAL = nMSISTEMAATUAL;
	}

	public String getNMSISTEMADONO() {
		return NMSISTEMADONO;
	}

	public void setNMSISTEMADONO(String nMSISTEMADONO) {
		NMSISTEMADONO = nMSISTEMADONO;
	}

	public String getDTALTERACAOEVENTO() {
		return DTALTERACAOEVENTO;
	}

	public void setDTALTERACAOEVENTO(String DTALTERACAOEVENTO) {
		this.DTALTERACAOEVENTO = DTALTERACAOEVENTO.substring(8, 10) + "/" + DTALTERACAOEVENTO.substring(5,7) + "/" + DTALTERACAOEVENTO.substring(0,4) +" "+ DTALTERACAOEVENTO.substring(11,13) +":"+ DTALTERACAOEVENTO.substring(14,15) + DTALTERACAOEVENTO.substring(15,16) +":"+ DTALTERACAOEVENTO.substring(17,19);
	}

	public String getDSTPREDE() {
		return DSTPREDE;
	}

	public void setDSTPREDE(String dSTPREDE) {
		DSTPREDE = dSTPREDE;
	}

	public String getDSEVENTO() {
		return DSEVENTO;
	}

	public void setDSEVENTO(String dSEVENTO) {
		DSEVENTO = dSEVENTO;
	}

	
	
	
}

	