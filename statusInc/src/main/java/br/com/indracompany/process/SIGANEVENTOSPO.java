package br.com.indracompany.process;


public class SIGANEVENTOSPO {
	
	private String prefixonumero;
	private String Dttransacao;
	private String idsistemaorigem;
	private String idsistemadestino;
	private String dserro;
	private String idfuncionalidade;
	
	
	
	public SIGANEVENTOSPO() {
		this.prefixonumero = "";
		this.Dttransacao = "";
		this.idsistemaorigem = "";
		this.idsistemadestino = "";
		this.dserro = "";
		this.idfuncionalidade = "";
	}
	public String getPrefixonumero() {
		return prefixonumero;
	}
	public void setPrefixonumero(String prefixonumero) {
		this.prefixonumero = prefixonumero;
	}
	public String getDttransacao() {
		return Dttransacao;
	}
	public void setDttransacao(String Dttransacao) {
		this.Dttransacao = Dttransacao.substring(8, 10) + "/" + Dttransacao.substring(5,7) + "/" + Dttransacao.substring(0,4) +" "+ Dttransacao.substring(11,13) +":"+ Dttransacao.substring(14,15) + Dttransacao.substring(15,16) +":"+ Dttransacao.substring(17,19);
	}
	public String getIdsistemaorigem() {
		return idsistemaorigem;
	}
	public void setIdsistemaorigem(String idsistemaorigem) {
		this.idsistemaorigem = idsistemaorigem;
	}
	public String getIdsistemadestino() {
		return idsistemadestino;
	}
	public void setIdsistemadestino(String idsistemadestino) {
		this.idsistemadestino = idsistemadestino;
	}
	public String getDserro() {
		return dserro;
	}
	public void setDserro(String dserro) {
		this.dserro = dserro;
	}
	public String getIdfuncionalidade() {
		return idfuncionalidade;
	}
	public void setIdfuncionalidade(String idfuncionalidade) {
		this.idfuncionalidade = idfuncionalidade;
	}
	
	
}
