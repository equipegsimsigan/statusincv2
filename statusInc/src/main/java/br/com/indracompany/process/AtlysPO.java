package br.com.indracompany.process;

import java.text.SimpleDateFormat;

public class AtlysPO {
	
	private String MSISDN;
	private String STATUS;
	private String TIPONUMERO;
	private String DATAATLYS;
	
	
	
	public AtlysPO() {
		this.MSISDN = "";
		this.STATUS = "";
		this.TIPONUMERO = "";
		this.DATAATLYS = "";
	}

	public String getMSISDN() {
		return MSISDN;
	}

	public void setMSISDN(String MSISDN) {
		this.MSISDN = MSISDN;
	}

	public String getSTATUS() {
		return STATUS;
	}

	public void setSTATUS(String STATUS) {
		this.STATUS = STATUS;
	}

	public String getTIPONUMERO() {
		return TIPONUMERO;
	}

	public void setTIPONUMERO(String TIPONUMERO) {
		this.TIPONUMERO = TIPONUMERO;
	}

	public String getDATAATLYS() {
		return DATAATLYS;
	}

	public void setDATAATLYS(String DATAATLYS) { 
		this.DATAATLYS = DATAATLYS.substring(6, 8) + "/" + DATAATLYS.substring(4,6) + "/" + DATAATLYS.substring(0,4);
	}
	
	
}